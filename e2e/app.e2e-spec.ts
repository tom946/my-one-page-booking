import { TrihotelPage } from './app.po';

describe('trihotel App', () => {
  let page: TrihotelPage;

  beforeEach(() => {
    page = new TrihotelPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
