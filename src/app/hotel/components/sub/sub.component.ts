import { StepsService } from './../../services/steps.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sub',
  templateUrl: './sub.component.html',
  styleUrls: ['./sub.component.css']
})
export class SubComponent implements OnInit {

  step: number;

  // Subscriptions
  subStepsService: Subscription;

  constructor(private stepsService: StepsService) { }

  ngOnInit() {
    this.subStepsService = this.stepsService.currentStep$.subscribe(step => this.step = step)
  }

  reStage(): void {
    this.stepsService.rePage()
  }

  avStage(): void {
    this.stepsService.avPage()
  }

  ngOnDestroy() {
    this.subStepsService.unsubscribe();
  }

}
