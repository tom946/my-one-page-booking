import { Subscription } from 'rxjs/Subscription';
import { BasketService } from './../../services/basket.service';
import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-first-step',
  templateUrl: './first-step.component.html',
  styleUrls: ['./first-step.component.css']
})
export class FirstStepComponent implements OnInit, OnDestroy {

  departure: Date;
  arrival: Date;

  // Childs
  @ViewChild('startCalendar') startCalendar: ElementRef
  @ViewChild('endCalendar') endCalendar: ElementRef

  // Subscriptions
  subArrival: Subscription;
  subDeparture: Subscription;

  constructor(private basketService: BasketService) { }

  ngOnInit() {
    this.subArrival = this.basketService.arrival$.subscribe(arrival => this.arrival = arrival);
    this.subDeparture = this.basketService.departure$.subscribe(departure => this.departure = departure);
  }

  ngAfterViewInit() {
    jQuery(this.startCalendar.nativeElement).calendar({
      onChange: (date, text, mode) => this.basketService.updateDeparture(date),
      minDate: new Date(),
      type: 'date',
      inline: true,
      today: true,
      endCalendar: jQuery(this.endCalendar.nativeElement),
    })
    jQuery(this.startCalendar.nativeElement).calendar('set date', this.departure, true, false);

    jQuery(this.endCalendar.nativeElement).calendar({
      onChange: (date, text, mode) => this.basketService.updateArrival(date),
      minDate: new Date(),
      type: 'date',
      inline: true,
      startCalendar: jQuery(this.startCalendar.nativeElement)
    })
    jQuery(this.endCalendar.nativeElement).calendar('set date', this.arrival, true, false);
  }

  ngOnDestroy() {
    this.subArrival.unsubscribe();
    this.subDeparture.unsubscribe();
  }

}
