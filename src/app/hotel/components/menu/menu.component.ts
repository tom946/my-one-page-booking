import { BasketService } from './../../services/basket.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  // language = "ES";
  // @ViewChild('dropdown') dropdown: ElementRef;

  constructor(public basketService: BasketService) { }

  ngOnInit() {
  //   jQuery(this.dropdown.nativeElement).dropdown({
  //     onChange: (a, b, c) => this.language = b
  //   })
  }

}
