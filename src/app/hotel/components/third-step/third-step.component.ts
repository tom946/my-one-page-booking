import { BillingInfo } from './../../classes/billing-info';
import { Passenger } from './../../classes/passenger';
import { Subscription } from 'rxjs/Subscription';
import { BasketService } from './../../services/basket.service';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-third-step',
  templateUrl: './third-step.component.html',
  styleUrls: ['./third-step.component.css']
})
export class ThirdStepComponent implements OnInit {

 passengers: Passenger[];

 // Subscriptions
 subPassengers: Subscription;

 constructor(private basketService: BasketService) {  }

  ngOnInit() { 
    this.subPassengers = this.basketService.passengers$.subscribe(
      passengers => this.passengers = passengers
    )
  }

  removePassenger(passenger: Passenger) {
    this.basketService.removePassenger(passenger)
  }

}
