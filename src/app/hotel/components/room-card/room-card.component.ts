import { Room } from './../../classes/room';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-room-card',
  templateUrl: './room-card.component.html',
  styleUrls: ['./room-card.component.css'],
  host: { 'class': 'ui raised link card'}
})
export class RoomCardComponent implements OnInit {

  @Input() room: Room;
  @Output('book') book = new EventEmitter();

  constructor() { }

  ngOnInit() {  }

  bookNow() {
    this.book.emit();
  }

}
