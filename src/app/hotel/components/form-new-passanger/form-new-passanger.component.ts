import { BasketService } from './../../services/basket.service';
import { Passenger } from './../../classes/passenger';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-new-passanger',
  templateUrl: './form-new-passanger.component.html',
  styleUrls: ['./form-new-passanger.component.css']
})
export class FormNewPassangerComponent implements OnInit {

  // Childs
  @ViewChild('calendar') calendar: ElementRef;
  @ViewChild('gender') gender: ElementRef;

  // form
  public form: FormGroup;

  constructor(
    private fb: FormBuilder,
    public basketService: BasketService
  ) {
    this.form = fb.group({
      firstName: ['', Validators.required],
      surname: ['', Validators.required],
      gender: ['', Validators.required],
      email: ['', Validators.required],
      document: ['', Validators.required],
      issuingCountry: ['', Validators.required],
      expiringDate: ['', Validators.required]
    })
  }

  ngOnInit() { }

  ngAfterViewInit(): void {
    jQuery(this.gender.nativeElement).dropdown();
    jQuery(this.calendar.nativeElement).calendar({
      type: 'date',
      onChange: (date, text, node) => {
        this.form.controls['expiringDate'].setValue(date)
      }
    })
  }

  onSend(): void {
    let passenger = new Passenger(
      this.form.value.firstName,
      this.form.value.surname,
      this.form.value.gender,
      this.form.value.email,
      this.form.value.document,
      this.form.value.issuingCountry,
      this.form.value.expiringDate
    )
    this.basketService.addPassenger(passenger);
    
    this.form.reset();
    jQuery(this.gender.nativeElement).dropdown('clear');
  }

}
