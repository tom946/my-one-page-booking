import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormNewPassangerComponent } from './form-new-passanger.component';

describe('FormNewPassangerComponent', () => {
  let component: FormNewPassangerComponent;
  let fixture: ComponentFixture<FormNewPassangerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormNewPassangerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormNewPassangerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
