import { BasketService } from './../../services/basket.service';
import { NotificationService } from './../../services/notification.service';
import { Room } from './../../classes/room';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-step',
  templateUrl: './second-step.component.html',
  styleUrls: ['./second-step.component.css']
})
export class SecondStepComponent implements OnInit {

  constructor(
    private notificationService: NotificationService,
    public basketService: BasketService
  ) { }

  ngOnInit() { }

  private booking(room: Room): void {
    if (room.quantity >= room.purchased + 1) {
      room.purchased = +room.purchased + 1;
      this.notificationService.addToast('Room added', `${room.name}: +${room.price}€`, 2)
      console.log(room)
    }
  }

}
