import { StepsService } from './../../services/steps.service';
import { Router } from '@angular/router';
import { BasketService } from './../../services/basket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  constructor(
    public basketService: BasketService,
    private router: Router,
    private stepsService: StepsService
  ) { }

  ngOnInit() {}

  onProcess() {
    this.router.navigate([''])
  }

}
