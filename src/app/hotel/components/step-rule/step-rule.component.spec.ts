import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepRuleComponent } from './step-rule.component';

describe('StepRuleComponent', () => {
  let component: StepRuleComponent;
  let fixture: ComponentFixture<StepRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
