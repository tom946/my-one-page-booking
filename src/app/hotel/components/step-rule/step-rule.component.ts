import { StepsService } from './../../services/steps.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-step-rule',
  templateUrl: './step-rule.component.html',
  styleUrls: ['./step-rule.component.css']
})
export class StepRuleComponent implements OnInit, OnDestroy {

  // Should be saved on a store (I guess is too much for )
  step: number;
  hotelId: string;


  // Subscriptions:
  subStepsService: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private stepsService: StepsService
  ) {
    // activatedRoute.queryParams
    //   .filter((params: Params) => Object.keys(params).length != 0)
    //   .subscribe((params: Params) => {
    //     this.hotelId = params['hotelId'];
    //     this.departure = new Date(params['departure']);
    //     this.arrival = new Date(params['arrival']);
    //   });
  }

  ngOnInit() {
    this.subStepsService = this.stepsService.currentStep$.subscribe(steps => this.step = steps)
  }

  ngOnDestroy() {
    this.subStepsService.unsubscribe();
  }
}
