import { Room } from './../../classes/room';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  @Input() rooms: Room[]

  constructor() { }

  ngOnInit() {}

  onRemove(room) {
    room.purchased = 0;
    return false;
  }

}
