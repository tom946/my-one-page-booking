import { NotificationService } from './../../services/notification.service';
import { BasketService } from './../../services/basket.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BillingInfo } from './../../classes/billing-info';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Input } from '@angular/core';

@Component({
  selector: 'app-form-client-info',
  templateUrl: './form-client-info.component.html',
  styleUrls: ['./form-client-info.component.css']
})
export class FormClientInfoComponent implements OnInit, AfterViewInit {

  form: FormGroup;
  billingInfo: BillingInfo = new BillingInfo();

  @ViewChild('country') country: ElementRef;
  @ViewChild('card') card: ElementRef;
  @ViewChild('expiration') expiration: ElementRef;
  @ViewChild('state') state: ElementRef;

  constructor(
    private fb: FormBuilder,
    private basketService: BasketService,
    private notificationService: NotificationService
  ) {

    if (basketService.billingInfo) {
      this.form = fb.group({
        firstName: [basketService.billingInfo.firstName, Validators.required],
        lastName: [basketService.billingInfo.lastName ? basketService.billingInfo.lastName : ''],
        address: [basketService.billingInfo.address, Validators.required],
        state: [basketService.billingInfo.state ? basketService.billingInfo.state : ''],
        country: [basketService.billingInfo.country, Validators.required],
        cardType: [basketService.billingInfo.cardType, Validators.required],
        cardNumber: [basketService.billingInfo.cardNumber, Validators.required],
        CVC: [basketService.billingInfo.CVC, Validators.required],
        expiration: [basketService.billingInfo.expiration, Validators.required],
        year: [basketService.billingInfo.year, Validators.required],
      });

    } else {
      this.form = fb.group({
        firstName: ['', Validators.required],
        lastName: [''],
        address: ['', Validators.required],
        state: [''],
        country: ['', Validators.required],
        cardType: ['', Validators.required],
        cardNumber: ['', Validators.required],
        CVC: ['', Validators.required],
        expiration: ['', Validators.required],
        year: ['', Validators.required],
      });
    }
    console.table(Object.keys(this.form.controls).map(x => [x, this.form.controls[x].value]))


  }

  ngOnInit() { }

  onSend() {
    this.basketService.billingInfo = this.form.value;
    this.notificationService.addToast('Success!', 'Billing account updated!', 2)
  }

  ngAfterViewInit() {
    jQuery(this.country.nativeElement).dropdown({
      forceSelection: false,
      onChange: (value, text, node) => this.form.controls['country'].setValue(node.text())
    });
    jQuery(this.card.nativeElement).dropdown({
      onChange: (value, text, node) => this.form.controls['cardType'].setValue(node.text())
    });
    jQuery(this.expiration.nativeElement).dropdown();
    jQuery(this.state.nativeElement).dropdown();

    if (this.basketService.billingInfo) {
      // set defaults
      setTimeout(() => {
        // There are an error about setting values of formcontrol after the form has been initzialited
        // https://github.com/angular/angular/issues/6005
        jQuery(this.expiration.nativeElement).dropdown('set selected', months[this.basketService.billingInfo.expiration])
        jQuery(this.card.nativeElement).dropdown('set selected', cardTypes[this.basketService.billingInfo.cardType])
        jQuery(this.state.nativeElement).dropdown('set selected', this.basketService.billingInfo.state)
        jQuery(this.country.nativeElement).dropdown('set selected', countries[this.basketService.billingInfo.country])

      })
    }
  }

}

const months = {
  January: 1,
  February: 2,
  March: 3,
  April: 4,
  May: 5,
  June: 6,
  July: 7,
  August: 8,
  September: 9,
  October: 10,
  November: 11,
  December: 12
}

const cardTypes = {
  "American Express": "amex",
  "Visa": "visa",
  "Discover": "discover"
}

const countries = {
  "Afghanistan": "af",
  "Aland Islands": "ax",
  "Albania": "al",
  "Algeria": "dz",
  "American Samoa": "as",
  "Andorra": "ad",
  "Angola": "ao",
  "Anguilla": "ai",
  "Antigua": "ag",
  "Argentina": "ar",
  "Armenia": "am",
  "Aruba": "aw",
  "Australia": "au",
  "Austria": "at",
  "Azerbaijan": "az",
  "Bahamas": "bs",
  "Bahrain": "bh",
  "Bangladesh": "bd",
  "Barbados": "bb",
  "Belarus": "by",
  "Belgium": "be",
  "Belize": "bz",
  "Benin": "bj",
  "Bermuda": "bm",
  "Bhutan": "bt",
  "Bolivia": "bo",
  "Bosnia": "ba",
  "Botswana": "bw",
  "Bouvet Island": "bv",
  "Brazil": "br",
  "British Virgin Islands": "vg",
  "Brunei": "bn",
  "Bulgaria": "bg",
  "Burkina Faso": "bf",
  "Burma": "mm",
  "Burundi": "bi",
  "Caicos Islands": "tc",
  "Cambodia": "kh",
  "Cameroon": "cm",
  "Canada": "ca",
  "Cape Verde": "cv",
  "Cayman Islands": "ky",
  "Central African Republic": "cf",
  "Chad": "td",
  "Chile": "cl",
  "China": "cn",
  "Christmas Island": "cx",
  "Cocos Islands": "cc",
  "Colombia": "co",
  "Comoros": "km",
  "Congo Brazzaville": "cg",
  "Congo": "cd",
  "Cook Islands": "ck",
  "Costa Rica": "cr",
  "Cote Divoire": "ci",
  "Croatia": "hr",
  "Cuba": "cu",
  "Cyprus": "cy",
  "Czech Republic": "cz",
  "Denmark": "dk",
  "Djibouti": "dj",
  "Dominica": "dm",
  "Dominican Republic": "do",
  "Ecuador": "ec",
  "Egypt": "eg",
  "El Salvador": "sv",
  "England": "gb",
  "Equatorial Guinea": "gq",
  "Eritrea": "er",
  "Estonia": "ee",
  "Ethiopia": "et",
  "European Union": "eu",
  "Falkland Islands": "fk",
  "Faroe Islands": "fo",
  "Fiji": "fj",
  "Finland": "fi",
  "France": "fr",
  "French Guiana": "gf",
  "French Polynesia": "pf",
  "French Territories": "tf",
  "Gabon": "ga",
  "Gambia": "gm",
  "Georgia": "ge",
  "Germany": "de",
  "Ghana": "gh",
  "Gibraltar": "gi",
  "Greece": "gr",
  "Greenland": "gl",
  "Grenada": "gd",
  "Guadeloupe": "gp",
  "Guam": "gu",
  "Guatemala": "gt",
  "Guinea-Bissau": "gw",
  "Guinea": "gn",
  "Guyana": "gy",
  "Haiti": "ht",
  "Heard Island": "hm",
  "Honduras": "hn",
  "Hong Kong": "hk",
  "Hungary": "hu",
  "Iceland": "is",
  "India": "in",
  "Indian Ocean Territory": "io",
  "Indonesia": "id",
  "Iran": "ir",
  "Iraq": "iq",
  "Ireland": "ie",
  "Israel": "il",
  "Italy": "it",
  "Jamaica": "jm",
  "Japan": "jp",
  "Jordan": "jo",
  "Kazakhstan": "kz",
  "Kenya": "ke",
  "Kiribati": "ki",
  "Kuwait": "kw",
  "Kyrgyzstan": "kg",
  "Laos": "la",
  "Latvia": "lv",
  "Lebanon": "lb",
  "Lesotho": "ls",
  "Liberia": "lr",
  "Libya": "ly",
  "Liechtenstein": "li",
  "Lithuania": "lt",
  "Luxembourg": "lu",
  "Macau": "mo",
  "Macedonia": "mk",
  "Madagascar": "mg",
  "Malawi": "mw",
  "Malaysia": "my",
  "Maldives": "mv",
  "Mali": "ml",
  "Malta": "mt",
  "Marshall Islands": "mh",
  "Martinique": "mq",
  "Mauritania": "mr",
  "Mauritius": "mu",
  "Mayotte": "yt",
  "Mexico": "mx",
  "Micronesia": "fm",
  "Moldova": "md",
  "Monaco": "mc",
  "Mongolia": "mn",
  "Montenegro": "me",
  "Montserrat": "ms",
  "Morocco": "ma",
  "Mozambique": "mz",
  "Namibia": "na",
  "Nauru": "nr",
  "Nepal": "np",
  "Netherlands Antilles": "an",
  "Netherlands": "nl",
  "New Caledonia": "nc",
  "New Guinea": "pg",
  "New Zealand": "nz",
  "Nicaragua": "ni",
  "Niger": "ne",
  "Nigeria": "ng",
  "Niue": "nu",
  "Norfolk Island": "nf",
  "North Korea": "kp",
  "Northern Mariana Islands": "mp",
  "Norway": "no",
  "Oman": "om",
  "Pakistan": "pk",
  "Palau": "pw",
  "Palestine": "ps",
  "Panama": "pa",
  "Paraguay": "py",
  "Peru": "pe",
  "Philippines": "ph",
  "Pitcairn Islands": "pn",
  "Poland": "pl",
  "Portugal": "pt",
  "Puerto Rico": "pr",
  "Qatar": "qa",
  "Reunion": "re",
  "Romania": "ro",
  "Russia": "ru",
  "Rwanda": "rw",
  "Saint Helena": "sh",
  "Saint Kitts and Nevis": "kn",
  "Saint Lucia": "lc",
  "Saint Pierre": "pm",
  "Saint Vincent": "vc",
  "Samoa": "ws",
  "San Marino": "sm",
  "Sandwich Islands": "gs",
  "Sao Tome": "st",
  "Saudi Arabia": "sa",
  "Senegal": "sn",
  "Serbia": "cs",
  "Seychelles": "sc",
  "Sierra Leone": "sl",
  "Singapore": "sg",
  "Slovakia": "sk",
  "Slovenia": "si",
  "Solomon Islands": "sb",
  "Somalia": "so",
  "South Africa": "za",
  "South Korea": "kr",
  "Spain": "es",
  "Sri Lanka": "lk",
  "Sudan": "sd",
  "Suriname": "sr",
  "Svalbard": "sj",
  "Swaziland": "sz",
  "Sweden": "se",
  "Switzerland": "ch",
  "Syria": "sy",
  "Taiwan": "tw",
  "Tajikistan": "tj",
  "Tanzania": "tz",
  "Thailand": "th",
  "Timorleste": "tl",
  "Togo": "tg",
  "Tokelau": "tk",
  "Tonga": "to",
  "Trinidad": "tt",
  "Tunisia": "tn",
  "Turkey": "tr",
  "Turkmenistan": "tm",
  "Tuvalu": "tv",
  "Uganda": "ug",
  "Ukraine": "ua",
  "United Arab Emirates": "ae",
  "United States": "us",
  "Uruguay": "uy",
  "Us Minor Islands": "um",
  "Us Virgin Islands": "vi",
  "Uzbekistan": "uz",
  "Vanuatu": "vu",
  "Vatican City": "va",
  "Venezuela": "ve",
  "Vietnam": "vn",
  "Wallis and Futuna": "wf",
  "Western Sahara": "eh",
  "Yemen": "ye",
  "Zambia": "zm",
  "Zimbabwe": "zw"
}