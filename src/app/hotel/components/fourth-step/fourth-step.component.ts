import { StepsService } from './../../services/steps.service';
import { NotificationService } from './../../services/notification.service';
import { Room } from './../../classes/room';
import { BasketService } from './../../services/basket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fourth-step',
  templateUrl: './fourth-step.component.html',
  styleUrls: ['./fourth-step.component.css']
})
export class FourthStepComponent implements OnInit {

  public rooms: Room[];

  constructor(
    public basketService: BasketService,
    public notificationService: NotificationService,
    public stepService: StepsService
  ) { }

  ngOnInit() {
    this.basketService.rooms$.subscribe(
      rooms => this.rooms = rooms
    )
  }
  

  public removeRoom(room: Room): void {
    if (this.isTheLastRoom()) {
      room.purchased--;
      this.rooms = this.rooms.slice(); // Pro-tip, we force to update the view generating a shadow copy! :P
    } else {
      this.notificationService.addToast('Error!', 'You can not remove the last room, you need one!!', 4)
    }
  }

  public calculateTotal(): number {
    return this.rooms.reduce((p, c) => p + (c.price * c.purchased), 0);
  }

  private isTheLastRoom(): boolean {
    return this.rooms.filter(r => r.purchased > 0).length > 1
  }


}
