import { BasketService } from './../../services/basket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-fifth-step',
  templateUrl: './fifth-step.component.html',
  styleUrls: ['./fifth-step.component.css']
})
export class FifthStepComponent implements OnInit {

  constructor(public basketService: BasketService) { }

  ngOnInit() {
  }

}
