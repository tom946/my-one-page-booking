import { BillingInfo } from './../classes/billing-info';
import { NotificationService } from './notification.service';
import { Passenger } from './../classes/passenger';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Room } from './../classes/room';
import { Injectable } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Injectable()
export class BasketService {

  // Hotel Info
  hotelName = new BehaviorSubject<string>('Random Hotel')
  hotelStars = new BehaviorSubject<number[]>([])

  // Dates
  private departure = new BehaviorSubject<Date>(new Date());
  public departure$ = this.departure.asObservable();
  private arrival = new BehaviorSubject<Date>(new Date((new Date()).valueOf() + 1000 * 3600 * 24)); // +1 day
  public arrival$ = this.arrival.asObservable();

  // Client Info
  public billingInfo: any;
  // Rooms
  private rooms: Room[] = [
    new Room('Single Room', 'Perfect for business or relax time', 'https://www.cleartrip.com/places/hotels/3789/378944/images/Business_Suite-Sitting_Area_w.jpg', 50, 3, 0),
    new Room('Double Room', 'Romantic date? Check this out!', 'https://www.cleartrip.com/places/hotels/7612/761256/images/Excutive_w.jpg', 100, 1, 0),
    new Room('Familiy Room', 'Enjoy your stance with family!', 'http://t-ec.bstatic.com/images/hotel/max1024x768/800/8004122.jpg', 150, 4, 0),
    new Room('Multiple Room', 'More people, more fun!', 'https://s-media-cache-ak0.pinimg.com/736x/e1/79/91/e17991f8d456de1c3689c5406f4061f4.jpg', 200, 1, 0)
  ];
  private _rooms = new BehaviorSubject<Room[]>(this.rooms);
  public rooms$ = this._rooms.asObservable();

  // Passengers
  private passengers: Passenger[] = [];
  private _passengers = new BehaviorSubject<Passenger[]>(this.passengers);
  public passengers$ = this._passengers.asObservable();


  constructor(
    private notificationService: NotificationService,
    private router: ActivatedRoute,
    private titleService: Title
  ) {
    router.queryParams
    .filter((params: Params) => Object.keys(params).length > 0)
    .subscribe((params: Params) => {
      try {
        // we try to parse the dates with our format
        this.hotelName.next(params['hotelName'])
        titleService.setTitle(params['hotelName'])
        let stars = +params['hotelStars'] 
        if (stars != undefined && !isNaN(stars)) {
          let array = []
          for(let i = 0; i < stars; i++) {
            array.push(i)
          }
          this.hotelStars.next(array)
        }
        this.hotelStars
        let departure = params['departure'].split('.')
        let newDeparture = new Date(20 + departure[2], departure[1] - 1, departure[0]);
        let arrival = params['arrival'].split('.')
        let newArrival = new Date(20 + arrival[2], arrival[1] - 1, arrival[0]);
        this.updateDeparture(newDeparture)
        this.updateArrival(newArrival)
      } catch (e) {
        console.log('Error parsing desired date')
      }

    });
  }
  

  public canWeAdvance(step: number): boolean {
    switch (step) {
      case 1:

        if (this.arrival.value == undefined || this.departure.value == undefined) {
          this.notificationService.addToast('Error!', 'You have to select atleast one departure date and one arrival date', 5);
          return false;
        }


        // obsly
        if (
          this.arrival.value < this.departure.value || this.departure.value > this.arrival.value
        ) {
          this.notificationService.addToast('Warning', 'Dates are incorrect!', 5)
          return false;
        } else {
          return true;
        }
      case 2:
        // atleast 1 room
        if (this.rooms.some(r => !r.isAvailable())) {
          return true;
        } else {
          this.notificationService.addToast('Info', 'Atleast has to choose one room!', 1)
          return false;
        }
      case 3:
        // atleast 1 passenger
        if (this.passengers.length < 1) {
          this.notificationService.addToast('Info', 'We need atleast one passanger', 1)
          return false
        }

        if (this.billingInfo == undefined) {
          this.notificationService.addToast('Info', 'We need an billing adress as well', 1);
          return false
        }

        return true
      case 4:
      case 5:
      case 6:
      default:
        return true;
    }
  }


  public updateArrival(date: Date): void {
    this.arrival.next(date);
  }

  public updateDeparture(date: Date): void {
    this.departure.next(date);
  }

  public addPassenger(passenger: Passenger) {
    this.passengers.push(passenger);
    this._passengers.next(this.passengers)
  }

  public removePassenger(passenger: Passenger) {
    let index = this.passengers.findIndex(r => r.email == passenger.email);
    if (index == undefined) return;

    this.passengers.splice(index, 1);
    this._passengers.next(this.passengers)
  }


}
