import { Injectable } from '@angular/core';
import { ToastyService, ToastyConfig } from "ng2-toasty";

@Injectable()
export class NotificationService {

  constructor(private toastyService: ToastyService, private toastyConfig: ToastyConfig) {
    this.toastyConfig.position = 'bottom-right';
    this.toastyConfig.theme = 'material';
    this.toastyConfig.showClose = true;
    this.toastyConfig.timeout = 3000;
  }

  addToast(title: string, msg: string, type: Number) {
    const toastOptions = { title: title, msg: msg };
    switch (type) {
      case 0: this.toastyService.default(toastOptions); break;
      case 1: this.toastyService.info(toastOptions); break;
      case 2: this.toastyService.success(toastOptions); break;
      case 3: this.toastyService.wait(toastOptions); break;
      case 4: this.toastyService.error(toastOptions); break;
      case 5: this.toastyService.warning(toastOptions); break;
    }
  }

}
