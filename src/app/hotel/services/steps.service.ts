import { BasketService } from './basket.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';


@Injectable()
export class StepsService {

  private currentStep = new BehaviorSubject(1);
  public currentStep$ = this.currentStep.asObservable();

  private minPage = 1;
  private maxPage = 6;

  constructor(private basketService: BasketService) { }

  public avPage() {
    if (this.currentStep.value + 1 <= this.maxPage && this.basketService.canWeAdvance(this.currentStep.value)) {
      this.currentStep.next(this.currentStep.value + 1)
    }
  }

  public rePage() {
    if (this.currentStep.value - 1 >= this.minPage) {
      this.currentStep.next(this.currentStep.value - 1)
    }
  }

  public setStep(step: number) {
    if (step < this.maxPage && step >= this.minPage) {
      this.currentStep.next(step);
    }
  }

  public getCurrentStep(): number {
    return this.currentStep.value;
  }

}
