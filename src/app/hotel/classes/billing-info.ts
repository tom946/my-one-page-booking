export class BillingInfo {
  firstName: string;
  lastName: string;
  address: string;
  state: string;
  country: string;
  cardType: "Visa" | "American Express" | "Discover";
  cardNumber: number;
  CVC: number;
  expiration: string;
  year: number;
}
