export class Passenger {
  constructor(
    public firstName: string,
    public surname: string,
    public gender: 'Male' | 'Female',
    public email: string,
    public document: string,
    public issuingCountry: string,
    public expiringDate: Date
  ) { }
}
