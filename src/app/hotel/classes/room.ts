export class Room {
  constructor(
    public name: string,
    public description: string,
    public image: string,
    public price: number,
    public quantity: number,
    public purchased: number,
  ) { }

  public isAvailable(): boolean {
    return this.quantity > this.purchased;
  }

}