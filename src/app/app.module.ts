import { BasketService } from './hotel/services/basket.service';
import { NotificationService } from './hotel/services/notification.service';
import { StepsService } from './hotel/services/steps.service';
import { AppRoutingModule } from './app-routing.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { StepsComponent } from './hotel/components/steps/steps.component';
import { SubComponent } from './hotel/components/sub/sub.component';
import { FirstStepComponent } from './hotel/components/first-step/first-step.component';
import { SecondStepComponent } from './hotel/components/second-step/second-step.component';
import { ThirdStepComponent } from './hotel/components/third-step/third-step.component';
import { FourthStepComponent } from './hotel/components/fourth-step/fourth-step.component';
import { FifthStepComponent } from './hotel/components/fifth-step/fifth-step.component';
import { RoomCardComponent } from './hotel/components/room-card/room-card.component';
import { MenuComponent } from './hotel/components/menu/menu.component';
import { CalendarComponent } from './shared/components/calendar/calendar.component';
import { FormClientInfoComponent } from './hotel/components/form-client-info/form-client-info.component';
import { ToastyModule } from 'ng2-toasty';
import { BasketComponent } from './hotel/components/basket/basket.component';
import { FormNewPassangerComponent } from './hotel/components/form-new-passanger/form-new-passanger.component';
import { PurchasedRoomsPipe } from './shared/pipes/purchased-rooms.pipe';
import { HeadersOfObjectPipe } from './shared/pipes/headers-of-object.pipe';
import { StepRuleComponent } from './hotel/components/step-rule/step-rule.component';
import { ProductTableComponent } from './hotel/components/product-table/product-table.component';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { PurchasedTotalPipe } from './shared/pipes/purchased-total.pipe';

@NgModule({
  declarations: [
    AppComponent,
    StepsComponent,
    SubComponent,
    FirstStepComponent,
    SecondStepComponent,
    ThirdStepComponent,
    FourthStepComponent,
    FifthStepComponent,
    RoomCardComponent,
    MenuComponent,
    CalendarComponent,
    FormClientInfoComponent,
    BasketComponent,
    FormNewPassangerComponent,
    PurchasedRoomsPipe,
    HeadersOfObjectPipe,
    StepRuleComponent,
    ProductTableComponent,
    DropdownDirective,
    PurchasedTotalPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
    ToastyModule.forRoot()
  ],
  providers: [
    StepsService,
    NotificationService,
    BasketService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
