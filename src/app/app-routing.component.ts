import { StepRuleComponent } from './hotel/components/step-rule/step-rule.component';
import { BasketComponent } from './hotel/components/basket/basket.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: StepRuleComponent
  },
  {
    path: 'basket',
    component: BasketComponent
  },
  {
    path: '**', 
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
