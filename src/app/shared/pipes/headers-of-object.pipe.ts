import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'headersOfObject'
})
export class HeadersOfObjectPipe implements PipeTransform {

  transform(value: Object, args?: any): any {
    if (value == null) return value;
    return Object.keys(value);
  }

}
