import { Room } from './../../hotel/classes/room';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'purchasedRooms',
  pure: false
})
export class PurchasedRoomsPipe implements PipeTransform {

  transform(rooms: Room[], args?: any): any {
    if (!rooms) return rooms;

    return rooms.filter((x => x.purchased > 0))
  }

}
