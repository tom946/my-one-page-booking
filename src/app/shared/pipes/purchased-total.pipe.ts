import { Room } from './../../hotel/classes/room';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'purchasedTotal',
  pure: false
})
export class PurchasedTotalPipe implements PipeTransform {

  transform(rooms: Room[], args?: any): any {
    if (!rooms) return rooms;

    return rooms
      .filter( room => room.purchased > 0)
      .reduce( (prev, curr) => prev + (curr.price * curr.purchased) , 0)
  }

}
