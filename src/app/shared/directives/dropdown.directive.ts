import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective {

  @Input() selected;
  @Input() item;

  constructor(el: ElementRef) {
    setTimeout(() => {
      jQuery(el.nativeElement).dropdown({
        forceSelection: false,
        onChange: (text, value, node) => this.item['purchased'] = text.trim()
      })
      jQuery(el.nativeElement).dropdown('set selected', this.selected)
    })
  }

}
