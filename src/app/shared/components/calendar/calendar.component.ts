import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter, OnChanges, AfterViewInit, SimpleChanges, NgZone } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit, AfterViewInit {

  @Input('currentDate') currentDate: Date;
  @Input() isStartDate: boolean;

  @Output() onChange = new EventEmitter();

  @ViewChild('calendar') calendar: ElementRef;

  constructor() { }

  ngOnInit() { }

  ngAfterViewInit() {
    let config = {
      onChange: (date, text, mode) => this.onChange.emit(date),
      minDate: new Date(),
      type: 'date',
      inline: true,
    }
    if (this.isStartDate) {
      config = Object.assign(config, { startCalendar: jQuery('#startDate') })
    } else {
      config = Object.assign(config, { endCalendar: jQuery('#endDate') })
    }
    jQuery(this.calendar.nativeElement).calendar(config);
    jQuery(this.calendar.nativeElement).calendar('set date', this.currentDate, true, false);
    console.log(config)
  }

}
